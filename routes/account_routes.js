var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');
var school_dal = require('../model/school_dal');
var company_dal = require('../model/company_dal');
var skill_dal = require('../model/skill_dal');


// View All account
router.get('/all', function(req, res) {
    account_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', { 'result':result });
        }
    });

});

// View the account for the given id
router.get('/', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.getInfo(req.query.account_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('account/accountViewById', {
                    'account': result[3][0], 'school': result[0], 'company':result[1], 'skill': result[2]
                });
            }
        });
    }
});

// Return the add a new account form
router.get('/add', function(req, res){
    school_dal.getAll(function(err,school) {
        if (err) {
            res.send(err);
        }
        else {
            company_dal.getAll(function(err, company) {
                if (err)
                    res.send(err);
                else
                {
                    skill_dal.getAll(function(err, skill) {
                        if (err) {
                            res.send(err)
                        }
                        else {
                            res.render('account/accountAdd', {
                                'school': school, 'company': company, 'skill': skill
                            });
                        }

                    });
                }
            });
        }
    });
});

// Insert the account for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.email== null) {
        res.send('Email must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        account_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/account/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.account_id == null) {
        res.send('An account id is required');
    }
    else {
        account_dal.getInfo(req.query.account_id, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                school_dal.getAll(function (err, school) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        company_dal.getAll(function (err, company) {
                            if (err)
                                res.send(err);
                            else {
                                skill_dal.getAll(function (err, skill) {
                                    if (err) {
                                        res.send(err)
                                    }
                                    else {
                                        res.render('account/accountUpdate', {
                                            'school': school, 'company': company, 'skill': skill,
                                            'account': result[3][0], 'account_school': result[0],
                                            'account_company': result[1], 'account_skill': result[2]
                                        });
                                    }

                                });
                            }
                        });
                    }
                });

            }
        });
    }
});


router.get('/update', function(req, res) {
    account_dal.update(req.query, function(err, result){
        res.redirect(302, '/account/all');
    });
});

// Delete a company for the given account_id
router.get('/delete', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.delete(req.query.account_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/account/all');
            }
        });
    }
});

module.exports = router;
